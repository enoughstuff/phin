class CreateAddUserTypeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_type, :string, default: "Investor"
  end
end
