class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :tagline
      t.integer :mbi #minimum buy-in
      t.integer :price
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip_code
      t.timestamps
    end
  end
end
