class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      # for all users - universal
      t.string  :first_name
      t.string  :last_name
      t.string  :address
      t.string  :city
      t.string  :state
      t.integer :zip_code
      t.string  :experience

      t.string  :company # Realtors and Managers required only

      # Investors only
      t.string  :industry
      t.integer :investable_assets
      
      # universal
      t.timestamps
    end
  end
end
