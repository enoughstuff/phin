# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Eclass Controller < ApplicationController
	# before_filter :find_model

	

	private
	def find_model
		@model = find(params[:id]) if params[:id]
	end

	# def img_from_url(url)
	# 	self.img=URI.parse(url)
	# 	self.img_file_name="avatar.png"
	# end
# endxamples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
many = 100
some = 10
types = ["Investor", "Realtor", "Manager"]
many*3.times do |i|

	selected_type = types[rand(0..2)]
	if selected_type == "Investor"
		user_industry = Faker::Commerce.department
		user_investable_assets = rand(10000000)
		user_company = nil
	else #if realtor or manager
		user_company = Faker::Company.name
		user_industry = nil
		user_investable_assets = nil
	end

	users=User.create!(
		first_name: Faker::Name.first_name,
		last_name: Faker::Name.last_name,
		email: Faker::Internet.email,
		password: Faker::Internet.password,
		address: Faker::Address.street_address,
		city: Faker::Address.city,
		state: Faker::Address.state,
		zip_code: Faker::Address.zip_code,
		experience: Faker::Lorem.paragraph,
		user_type: selected_type,
		company: user_company,
		industry: user_industry,
		investable_assets: user_investable_assets, 
		avatar: URI.parse("http://lorempixel.com/200/200/people/")
  )
end

many.times do |i|
	listings=Listing.create!(
		tagline: Faker::Lorem.sentence,
    mbi: 100 + rand(900),
    price: 10000 + rand(10000000),
		address: Faker::Address.street_address,
    city: Faker::Address.city,
    state: Faker::Address.state,
    zip_code: Faker::Address.zip_code,
  	avatar: URI.parse("http://lorempixel.com/200/200/people/")
	)
end

some.times do |i|
	events=Event.create!(
		name: Faker::Lorem.sentence,
  	address: Faker::Address.street_address,
  	city: Faker::Address.city,
  	state: Faker::Address.state,
  	zip_code: Faker::Address.zip_code,
  	date: Date.new(Time.now.year, 1 + rand(11), 1 + rand(27)),
   	time: rand(1440.0)/60.0,
		description:Faker::Lorem.sentence
	)
end