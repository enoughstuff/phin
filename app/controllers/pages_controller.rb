class PagesController < ApplicationController
  layout 'pages'
  # This controller would be responsible for rendering pages that are secondary to resource pages.
  def index

  end

  def about_us
  end

  def faq
  end
end
