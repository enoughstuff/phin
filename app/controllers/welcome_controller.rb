class WelcomeController < ApplicationController
  def index
    render 'index', layout: false 
  end

  def about_us
    render 'about_us'
  end

end
