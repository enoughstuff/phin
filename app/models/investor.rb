class Investor < User
	has_many :managers, through: :listings
	has_many :realtors, through: :listings
	has_many :events, through: :listings
end