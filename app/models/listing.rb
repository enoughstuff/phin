class Listing < ActiveRecord::Base
  belongs_to :realtor
  has_one :manager
  has_many :investors
  has_many :events  

  has_attached_file :avatar
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
end
