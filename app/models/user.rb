class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	#enum user_type: [ :realtor, :manager, :investor ]

	scope :investors, -> { where(user_type: 'Investor') }
	scope :managers,  -> { where(user_type: 'Manager') }
	scope :realtors,  -> { where(user_type: 'Realtor') }

	#set_inheritance_column :user_type /investors/
	self.inheritance_column = :user_type

	user_types = ['Investor', 'Manager', 'Realtor']

	has_many :listings

  has_attached_file :avatar
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

end
