class Realtor < User
	has_many :investors, through: :listings
	has_many :managers, through: :listings
	has_many :events, through: :listings
end