class Manager < User
	has_many :investors, through: :listings
	has_many :realtors, through: :listings
	has_many :events, through: :listings

end