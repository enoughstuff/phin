Rails.application.routes.draw do
  #resources :users
  devise_for :users

  resources :events

  resources :listings
  #get 'listings/welcome', controller: 'listings', action: 'welcome'

  resources :investors, controller: 'users', user_type: 'Investor'
  resources :managers, controller: 'users', user_type: 'Manager'
  resources :realtors, controller: 'users', user_type: 'Realtor'

  get 'about_us', controller: 'pages'
  get 'faq', controller: 'pages'

  root 'pages#index'

  
end
